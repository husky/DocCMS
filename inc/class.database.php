<?php
/*
 * author :Mindci Brand Planning<tel:400-9210-146>
 * @copyright ©2020 i8home-CMS , www.i8home.com, Mindci Brand Planning :)
 */
define('ARRAY_A', PDO::FETCH_ASSOC, false);
define('ARRAY_N', PDO::FETCH_OBJ, false);
class DocDB{
	public $show_errors = true;
	public $db;
	public static $dbStatement=NULL;
	public function __construct(){
		$this->configfile=ABSPATH.'/config/doc-config-'.$_SESSION['doclang'].'.php';
		$this->db=$this->connectDb();
		$this->db->query('set names utf8');
		$this->init();
	}
	public function init(){
	}
	protected function connectDb(){	
	try {  
            $instance=new PDO('mysql:dbname='.DB_DBNAME.';host='.DB_HOSTNAME.';port=3306',DB_USER,DB_PASSWORD);
            $instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $instance;
        } catch (PDOException $e) {  
			$this->bail("<h1>建立数据库链接时出错！</h1>
		<p>也可能是存在于<code>doc-config.php</code>文件中的数据库用户名或密码不正确，不能建立与数据库服务器<code>$dbhost</code>的连接. </p>
		<ul>
			<li>你确定你的数据库用户名密码没错？</li>
			<li>你确定你输入了正确的主机名？</li>
			<li>你确定你的数据库服务器正在运行？</li>
		</ul>
		<p>如果你不确定这些信息请联系你的虚拟主机服务供应商. 如果仍然不能解决请登陆 <a href='http://www.doccms.net/'>麦研i8HOME-CMS官方技术支持论坛</a>.</p>");
        }  
	}
	public final function query($sql){
		return $this->db->query($sql);
	}
	public final function exec($sql){
		return $this->db->exec($sql);
	}
	public final function prepare ($statment,$option=array()) {
		return self::$dbStatement=$this->db->prepare($statment,$option);
	}
	public final function execute($input_parameters=array()){
		return self::$dbStatement->execute($input_parameters=array());
	}
	public final function beginTransaction () {
		return $this->db->beginTransaction();
	}
	public final function commit () {
		return $this->db->commit();
	}
	public final function rollBack () {
		return $this->db->rollBack();
	}
	public final function setAttribute ($attribute, $value) {
		return $this->db->setAttribute($attribute,$value);
	}
	public final function lastInsertId($seqname='') {
		return $this->db->lastInsertId($seqname);
	}
	public final function errorCode () {
		return $this->db->errorCode();
	}
	public final function errorInfo () {
		return $this->db->errorInfo();
	}
	public final function getAttribute ($attribute) {
		return $this->db->getAttribute($attribute);
	}
	/**
	* 字符串过滤
	*/
	public final function quote($string, $paramtype='') {
		return $this->db->quote($string,$paramtype);
	}
	/**
	 *
	 * 返回一行数据集
	 */
	public final function get_var($sql='',$type=PDO::FETCH_OBJ){
		if(empty(self::$dbStatement)){
			return $this->db->query($sql)->fetchColumn();
		}
		else{
			return self::$dbStatement->fetchColumn();
		}	
	}
    /**
     *
     * 返回一列的数据集
     */
    public final function get_col($sql='',$type=PDO::FETCH_OBJ){
        if(empty(self::$dbStatement)){
            $result = $this->db->query($sql)->fetchAll($type);
            return array_map('reset',$result);

        }
        else{
            $result =  self::$dbStatement->fetchAll($type);
            return array_map('reset',$result);
        }
    }
    /**
	 *
	 * 返回一行数据集
	 */
	public final function get_row($sql='',$type=PDO::FETCH_OBJ){
		if(empty(self::$dbStatement)){
			return $this->db->query($sql)->fetch($type);
		}
		else{
			return self::$dbStatement->fetch($type);
		}
	}
	/**
	 *
	 * 返回多行数据集
	 */
	public final function get_results($sql='',$type=PDO::FETCH_OBJ){
		if(empty(self::$dbStatement)){
			$result = $this->db->query($sql)->fetchAll($type);
			$this->getPDOError();
			return $result ;
		}
		else{
			return self::$dbStatement->fetchAll($type);
		}
	}
	/**
	 *
	 * 返回列数
	 */
	public final function columnCount(){
		return self::$dbStatement->columnCount();
	}
	/**
	 *
	 * 返回行数
	 */
	public final function rowCount(){
		return self::$dbStatement->rowCount();
	}
	/**
	 * 返回count
	 */
	public final function numRows($table,$condition){
		return $this->db->query("SELECT count(*) FROM `{$table}` WHERE {$condition}")->fetchColumn();
	}
	/**
	 *
	 * 捕获错误信息
	 */
	private function getPDOError()
    {
        if ($this->db->errorCode() != '00000') {
            $arrayError = $this->db->errorInfo();
            $this->outputError($arrayError[2]);
        }
    }
	 /**
     * 输出错误信息
     * 
     * @param String $strErrMsg
     */
    private function outputError($strErrMsg)
    {
        throw new Exception('MySQL Error: '.$strErrMsg);
    }
	/**
     * destruct 关闭数据库连接
     */
    public function destruct()
    {
        $this->db = null;
    }
	public final function bail($message) { // Just wraps errors in a nice header and footer
		if ( !$this->show_errors )
		return false;
		header( 'Content-Type: text/html; charset=utf-8');
		echo <<<HEAD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>
麦研新媒体i8home-CMS系统企业建站系统 错误页面</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style media="screen" type="text/css">
		<!--
		html {
			background: #eee;
		}
		body {
			background: #fff;
			color: #000;
			font-family: Georgia, "Times New Roman", Times, serif;
			margin-left: 25%;
			margin-right: 25%;
			padding: .2em 2em;
		}
		
		h1 {
			color: #006;
			font-size: 18px;
			font-weight: lighter;
		}
		
		h2 {
			font-size: 16px;
		}
		
		p, li, dt {
			line-height: 140%;
			padding-bottom: 2px;
		}
	
		ul, ol {
			padding: 5px 5px 5px 20px;
		}
		#logo {
			margin-bottom: 2em;
		}
		-->
		</style>
	</head>
	<body>
	<h1 id="logo"><a href="http://www.i8home.com">DOCCMS</a></h1>
HEAD;
		echo "<div><font color=\"Red\">出现这种错误的原因是可能您还没有安装数据库，请点击<a href='/setup/setup.php'>安装</a>您的数据库</font></div>";
		echo $message;
		echo "</body></html>";
		die();
	}
}
class DtDatabase extends DocDB {

	function loadDB() {
		$this->configfile=ABSPATH.'/config/doc-config-'.$_SESSION['doclang'].'.php';
		$this->db=$this->connectDb();
		$this->db->query('set names utf8');
		$this->init();
	}
}
$db = new DocDB();